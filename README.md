# Tidy Transit Headway analysis for the Australian National Liveability Study 2018

An analysis of day time weekday public transport service frequency and maximum interdeparture time, for the Australian National Liveability Study 2018, using Australian GTFS transport feed data for each state and territory.

Carl Higgs and Alan Both, 2020